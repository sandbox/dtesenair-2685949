<?php
/* Default theme implementation to display a single Drupal page.
 *
 * The doctype, html, head and body tags are not in this template. Instead they can be found in the
 * html.tpl.php template in this directory.
 *
 * Available variables:
 *
 * General utility variables:
 * $base_path: The base URL path of the Drupal installation. At the very least, this will always default to /.
 * $directory: The directory the template is located in, e.g. modules/system or themes/bartik.
 * $is_front: TRUE if the current page is the front page.
 * $logged_in: TRUE if the user is registered and signed in.
 * $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * $front_page: The URL of the front page. Use this instead of $base_path, when linking to the front page. This includes the language domain or prefix.
 * $logo: The path to the logo image, as defined in theme configuration.
 * $site_name: The name of the site, empty when display has been disabled in theme settings.
 * $site_slogan: The slogan of the site, empty when display has been disabled in theme settings.
 *
 * Navigation:
 * $main_menu (array): An array containing the Main menu links for the site, if they have been configured.
 * $secondary_menu (array): An array containing the Secondary menu links for the site, if they have been configured.
 * $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * $title_prefix (array): An array containing additional output populated by modules, intended to be displayed in front of the main title tag that appears in the template.
 * $title: The page title, for use in the actual HTML content.
 * $title_suffix (array): An array containing additional output populated by modules, intended to be displayed after the main title tag that appears in the template.
 * $messages: HTML for status and error messages. Should be displayed prominently.
 * $tabs (array): Tabs linking to any sub-pages beneath the current page (e.g., the view and edit tabs when displaying a node).
 * $action_links (array): Actions local to the page, such as 'Add menu' on the menu administration interface.
 * $feed_icons: A string of all feed icons for the current page.
 * $node: The node object, if there is an automatically-loaded node associated with the page, and the node ID is the second argument in the page's path (e.g. node/12345 and node/12345/revisions, but not comment/reply/12345).
 *
 * Regions:
 * $page['help']: Dynamic help text, mostly for admin pages.
 * $page['highlighted']: Items for the highlighted content region.
 * $page['content']: The main content of the current page.
 * $page['sidebar_first']: Items for the first sidebar.
 * $page['sidebar_second']: Items for the second sidebar.
 * $page['header']: Items for the header region.
 * $page['footer']: Items for the footer region.
 */
?>



<div id="header-wrap">
  <div id="header-left">
		<div id="logo">
			<?php if ($logo): ?>
			  <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo">
				<!-- <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>"/> -->
				<img src="<?php print '/' . drupal_get_path('theme', $GLOBALS['theme']) . '/logo1.png'; ?>" alt="<?php print t('Home'); ?>" class="logo1"/>
				<img src="<?php print '/' . drupal_get_path('theme', $GLOBALS['theme']) . '/logo2.png'; ?>" alt="<?php print t('Home'); ?>" class="logo2"/>
			  </a>
			<?php endif; ?>
			
			<?php if (theme_get_setting('aldersgate_default_logo')==1 ): ?>
				<div id="alt-logo">
				  <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>">
					<img src="<?php theme_get_setting('aldersgate_default_logo'); ?>" alt="<?php print t('Home'); ?>" />
					<img src="logo.png" alt="<?php print t('Home'); ?>" />
				  </a>
				</div>
		  <?php endif; ?>	  
		</div>

		<div id="site-name">	
			<?php if ($site_name): ?>
				<h1><a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><?php print $site_name; ?></a></h1>
			<?php endif; ?>
		</div>
	
		<div id="site-slogan">
			<?php if ($site_slogan): ?>
				<h2><?php print $site_slogan; ?></h2>
			<?php endif; ?>
		</div>
	</div>
	
	<div id="header-right">
		<?php if (theme_get_setting('display_social_icons')): ?>
			<?php
      $twitter_url = check_plain(theme_get_setting('twitter_url'));
      $facebook_url = check_plain(theme_get_setting('facebook_url'));
      $google_plus_url = check_plain(theme_get_setting('google_plus_url'));
      $pinterest_url = check_plain(theme_get_setting('pinterest_url'));
			?>
			<div id="social-icons">
				<ul>
					<?php if ($facebook_url): ?>
						<li><a target="_blank" title="<?php print $site_name; ?> on Facebook" href="<?php print $facebook_url; ?>">
								<i class="fa fa-facebook"></i> 
							</a></li>
            <?php endif; ?>
            <?php if ($twitter_url): ?>
              <li><a target="_blank" title="<?php print $site_name; ?> on Twitter" href="<?php print $twitter_url; ?>">
                  <i class="fa fa-twitter"></i> 
                </a></li>
            <?php endif; ?>
            <?php if ($google_plus_url): ?>
              <li><a target="_blank" title="<?php print $site_name; ?> on Google+" href="<?php print $google_plus_url; ?>">
                  <i class="fa fa-google-plus"></i>
                </a></li>
            <?php endif; ?>
            <?php if ($pinterest_url): ?>
              <li><a target="_blank" title="<?php print $site_name; ?> on Pinterest" href="<?php print $pinterest_url; ?>">
                  <i class="fa fa-pinterest-p"></i> 
                </a></li>
            <?php endif; ?>
            <li><a target="_blank" title="<?php print $site_name; ?> via RSS" href="<?php print $front_page; ?>rss.xml">
                <i class="fa fa-rss"></i>
              </a></li>
          </ul>
        </div>
		
      <?php endif; ?>

	  
  	<div id="secondary-menu">
      <?php if ($logged_in): ?>
        <?php if ($secondary_menu): ?>
          <?php print theme('links', array('links' => $secondary_menu, 'attributes' => array('id' => 'secondary', 'class' => array('links', 'clearfix', 'sub-menu')))); ?>
        <?php endif; ?>
      <?php else: ?>
          <ul id="login-links">
            <li><a href="/user">Login</a></li>
            <li><a href="/user/register">Register</a></li>
          </ul>
      <?php endif; ?>
	  </div>
    	  
	</div>
	 
 	
	
	<div id="menu-wrap" >
		<?php if ($main_menu): ?>
		  <?php //print theme('links', array('links' => $main_menu, 'attributes' => array('id' => 'primary', 'class' => array('links', 'clearfix', 'main-menu')))); ?>
      <?php
      $main_menu_tree = menu_tree(variable_get('menu_main_links_source', 'main-menu'));
      print drupal_render($main_menu_tree);
        ?>
		<?php endif; ?>
	</div>


  


</div>

  <?php if ($page['header']): ?>
    <?php print render($page['header']); ?>
  <?php endif; ?>



  <!-- RENDER TITLE JUST BELOW HEADER IF WE ARE **NOT** ON THE FRONT PAGE -->
  <?php if (!$is_front && $title): ?>
    
    <?php 
      //determine which image we should be using as the background
      if ( theme_get_setting('title_wrap_use_default_image') == 0 && theme_get_setting('title_wrap_image') != 0 ) {
        $fid = theme_get_setting('title_wrap_image');
        $image_url = file_create_url(file_load($fid)->uri);
      } else {
        $image_url = '/' . drupal_get_path('theme',$GLOBALS['theme']) . '/images/title-bg.jpg';  
      }

      //now, determine if we should enable the parallax attributes
      if ( theme_get_setting('title_wrap_parallax_enable') == 1  ) {
        $speed = theme_get_setting('title_wrap_parallax_speed');
        $position = theme_get_setting('title_wrap_parallax_position');
        $attrs = 'class="parallax-window" data-parallax="scroll" data-position="' . $position . '" data-speed="' . $speed . '" data-image-src="' . $image_url . '"';
      } else {
        //no parallax enabled so simply set the background image via the "style" attribute
        $attrs = 'style="background-image: url(' . $image_url . '); background-size: cover;"';        
      }
    ?>
    
  
    <div id="title-wrap" <?php print $attrs; ?>>
      
    	<?php print render($title_prefix); ?>
    	
    	<?php if ($title): ?>
    	  <h1><?php print $title; ?></h1>
    	<?php endif; ?>
    
    	<?php print render($title_suffix); ?>
    
    </div>
  <?php endif; ?>


<?php
  if ( theme_get_setting('breadcrumbs')==1 && !$is_front)
    print $breadcrumb;
 ?>





    <?php if ($page['highlight']): ?>
      <?php 
        //determine which image we should be using as the background
        if ( theme_get_setting('highlight_use_default_image') == 0 && theme_get_setting('highlight_image') != 0 ) {
          $fid = theme_get_setting('highlight_image');
          $image_url = file_create_url(file_load($fid)->uri);
        } else {
          $image_url = '/' . drupal_get_path('theme',$GLOBALS['theme']) . '/images/highlight-bg.jpg';  
        }
  
        //now, determine if we should enable the parallax attributes
        if ( theme_get_setting('highlight_parallax_enable') == 1  ) {
          $speed = theme_get_setting('highlight_parallax_speed');
          $position = theme_get_setting('highlight_parallax_position');
          $attrs = 'class="parallax-window" data-parallax="scroll" data-position="' . $position . '" data-speed="' . $speed . '" data-image-src="' . $image_url . '"';
        } else {
          //no parallax enabled so simply set the background image via the "style" attribute
          $attrs = 'style="background-image: url(' . $image_url . '); background-size: cover;"';        
        }
      ?>
      <div id="highlight-wrap" <?php print $attrs; ?>>
          <?php print render($page['highlight']) ?>
      </div>
    <?php endif; ?>





<?php print $messages; ?>
<?php print render($page['help']); ?>



 


<div id="content-wrap">

  <div id="content" class="<?php if ($page['sidebar']) print 'sidebar'; ?>">

    <!-- ONLY ADD TITLE IN CONTENT REGION IF WE ARE ON THE FRONT PAGE -->
    <?php if ($is_front && $title): ?>
      <div id="title-wrap">
        <?php print render($title_prefix); ?>
        
        <?php if ($title): ?>
          <h1><?php print $title; ?></h1>
        <?php endif; ?>
      
        <?php print render($title_suffix); ?>
      
      </div>
    <?php endif; ?>


  	<?php if ($tabs): ?>
  	  <?php print render($tabs); ?>
  	<?php endif; ?>
  	
  	<?php if ($action_links): ?>
  	  <ul><?php print render($action_links); ?></ul>
  	<?php endif; ?>
  	
  	<?php print render($page['content']) ?>
    <?php //print $feed_icons; ?>
  </div>

  <?php if ($page['sidebar'] && !$is_front): ?>
    <div id="sidebar">
     <?php print render($page['sidebar']); ?>
    </div>
  <?php endif; ?>

</div>


<?php if ($page['bottom_highlight1']): ?>
  <?php 
    //determine which image we should be using as the background
    if ( theme_get_setting('bottom_highlight1_use_default_image') == 0 && theme_get_setting('bottom_highlight1_image') != 0 ) {
      $fid = theme_get_setting('bottom_highlight1_image');
      $image_url = file_create_url(file_load($fid)->uri);
    } else {
      $image_url = '/' . drupal_get_path('theme',$GLOBALS['theme']) . '/images/bottom-highlight1-bg.jpg';  
    }

    //now, determine if we should enable the parallax attributes
    if ( theme_get_setting('bottom_highlight1_parallax_enable') == 1  ) {
      $speed = theme_get_setting('bottom_highlight1_parallax_speed');
      $position = theme_get_setting('bottom_highlight1_parallax_position');
      $attrs = 'class="parallax-window" data-parallax="scroll" data-position="' . $position . '" data-speed="' . $speed . '" data-image-src="' . $image_url . '"';
    } else {
      //no parallax enabled so simply set the background image via the "style" attribute
      $attrs = 'style="background-image: url(' . $image_url . '); background-size: cover;"';        
    }

  ?>
    <div id="bottom-highlight1-wrap" <?php print $attrs; ?>>
      <?php print render($page['bottom_highlight1']) ?>
    </div>
<?php endif; ?>
 




<?php if ($page['bottom_highlight2']): ?>
  <?php 
    //determine which image we should be using as the background
    if ( theme_get_setting('bottom_highlight2_use_default_image') == 0 && theme_get_setting('bottom_highlight2_image') != 0 ) {
      $fid = theme_get_setting('bottom_highlight2_image');
      $image_url = file_create_url(file_load($fid)->uri);
    } else {
      $image_url = '/' . drupal_get_path('theme',$GLOBALS['theme']) . '/images/bottom-highlight2-bg.jpg';  
    }

    //now, determine if we should enable the parallax attributes
    if ( theme_get_setting('bottom_highlight2_parallax_enable') == 1  ) {
      $speed = theme_get_setting('bottom_highlight2_parallax_speed');
      $position = theme_get_setting('bottom_highlight2_parallax_position');
      $attrs = 'class="parallax-window" data-parallax="scroll" data-position="' . $position . '" data-speed="' . $speed . '" data-image-src="' . $image_url . '"';
    } else {
      //no parallax enabled so simply set the background image via the "style" attribute
      $attrs = 'style="background-image: url(' . $image_url . '); background-size: cover;"';        
    }
  ?>
    <div id="bottom-highlight2-wrap" <?php print $attrs; ?>>
      <?php print render($page['bottom_highlight2']) ?>
    </div>
<?php endif; ?>


<div id="footer-wrap">
  <?php if ($page['footer']): ?>
    <?php print render($page['footer']); ?>
  <?php endif; ?>
  

  <?php if ($page['copyright']): ?>
    <?php print render($page['copyright']); ?>
  <?php endif; ?>


  <?php if (theme_get_setting('scroll_to_top')==1 ): ?>
    <a href="#" id="scroll-to-top">Scroll</a>
  <?php endif; ?>


</div>


