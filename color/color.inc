<?php

// Put the logo path into JavaScript for the live preview.
drupal_add_js(array('color' => array('logo' => theme_get_setting('logo', 'aldersgate'))), 'setting');

$info = array(
  // Available colors and color labels used in theme.
  'fields' => array(
    'base' => t('Base Color'),
    'text' => t('Body Text'),
    'link' => t('Body Links'),

        //'headertext' => t('Header Text'),
    //'headertextscrolled' => t('Header Text (Scrolled)'),

    //'background' => t('Body Background'),
    //'headings' => t('Body Headings'),

  	//'footerback' => t('Footer Background'),
  	//'footerheadings' => t('Footer Headings'),
  	//'footertext' => t('Footer Text'),
  	//'footerlinks' => t('Footer Links'),

  ),
  // Pre-defined color schemes.
  'schemes' => array(
    'default' => array(
      'title' => t('Chrysalis Blue (default)'),
      'colors' => array(
        'base' => '#02aace',
        'text' => '#3b3b3b',
        'link' => '#02aacd',
        
        //'headertext' => '#fffffe',
        //'headertextscrolled' => '#333333',
 
        //'background' => '#f8f8f8',
        //'headings' => '#ffffff',
 
        //'footerback' => '#111111',
        //'footerheadings' => '#ffffff',
        //'footertext' => '#cccccc',
        //'footerlinks' => '#02aacc',
      ),
    ),
  
    'emmaus' => array(
      'title' => t('Emmaus Red'),
      'colors' => array(
        'base' => '#c63d0f',
        'text' => '#3b3b3b',
        'link' => '#c9c22f',
        
        //'headertext' => '#ffffff',
        //'headertextscrolled' => '#333333',
 
        //'background' => '#f8f8f8',
        ///'headings' => '#ffffff',
 
        //'footerback' => '#111111',
        //'footerheadings' => '#ffffff',
        //'footertext' => '#cccccc',
        //'footerlinks' => '#02aace',
      ),
    ),


  ),

  // CSS files (excluding @import) to rewrite with new color scheme.
  'css' => array(
    'color/color2.css',
  ),

  // Files to copy.
  'copy' => array(),

  // Gradient definitions.
  'gradients' => array(),

  // Color areas to fill (x, y, width, height).
  'fill' => array(),

  // Coordinates of all the theme slices (x, y, width, height)
  // with their filename as used in the stylesheet.
  'slices' => array(),

  // Reference color used for blending. Matches the base.png's colors.
  'blend_target' => '#ffffff',

  // Preview files.
  'preview_css' => 'color/preview.css',
  'preview_js' => 'color/preview.js',
  'preview_html' => 'color/preview.html',

  // Base file for image generation.
  'base_image' => 'color/base.png',
);
