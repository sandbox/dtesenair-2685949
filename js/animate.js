jQuery(function($) {

  //Initialize the Superfish main menu
  $(document).ready(function() {

    var animateUp = '.front #title-wrap h1';
        animateUp += ', #block-block-9 .ld-photo img';
    var animateDown = '#block-block-9 .block-title';
    var animateLeft = '.front #block-views-upcoming_events-block .title-content';
        animateLeft += ',.front #block-views-latest_articles-block .title-content';
        animateLeft += ', #block-views-walk_countdown-block .left';
        animateLeft += ', #block-block-9 .content';
    var animateRight = '#block-views-walk_countdown-block .right';
    
    $(animateUp).addClass("animated");
    $(animateDown).addClass("animated");
    $(animateLeft).addClass("animated");
    $(animateRight).addClass("animated");


    //ADD CLASS TO "ANIMATE" ELEMENTS
    $(window).scroll( function(){    
      
        //fadeInLeft      
        $(animateLeft).each( function(i){
            var bottom_of_object = $(this).offset().top + $(this).outerHeight();
            var bottom_of_window = $(window).scrollTop() + $(window).height();
            
            /* If the object is completely visible in the window, fade it in */
            if( bottom_of_window > bottom_of_object ){
                $(this).addClass("animated fadeInLeft");
            }
        }); 
        ///fadeInRight
        $(animateRight).each( function(i){
            $(this).addClass("animate");
            var bottom_of_object = $(this).offset().top + $(this).outerHeight();
            var bottom_of_window = $(window).scrollTop() + $(window).height();
            
            /* If the object is completely visible in the window, fade it in */
            if( bottom_of_window > bottom_of_object ){
                $(this).addClass("animated fadeInRight");
            }
        }); 

        //fadeInUp
        $(animateUp).each( function(i){
            var bottom_of_object = $(this).offset().top + $(this).outerHeight();
            var bottom_of_window = $(window).scrollTop() + $(window).height();
            
            /* If the object is completely visible in the window, fade it in */
            if( bottom_of_window > bottom_of_object ){
                $(this).addClass("fadeInUp");
            }
        }); 

        //fadeInDown
        $(animateDown).each( function(i){
            var bottom_of_object = $(this).offset().top + $(this).outerHeight();
            var bottom_of_window = $(window).scrollTop() + $(window).height();
            
            /* If the object is completely visible in the window, fade it in */
            if( bottom_of_window > bottom_of_object ){
                $(this).addClass("fadeInDown");
            }
        }); 

        
    }); //window.scroll
 


  }); //end $(document).ready

});
