<?php


/**
 * Implements hook_form_FORM_ID_alter().
 *
 * @param $form
 *   The form.
 * @param $form_state
 *   The form state.
 */
function aldersgate_form_system_theme_settings_alter(&$form, &$form_state) {

  //drupal_set_message('<pre>' . print_r($form, TRUE) . '</pre>');
  //drupal_set_message('<pre>' . theme_get_setting('default_alternate_logo') . '</pre>');
  
  $form['aldersgate_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Aldersgate Theme Settings'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );
  $form['aldersgate_settings']['clear_registry'] = array(
    '#type' => 'checkbox',
    '#title' =>  t('Rebuild theme registry on every page.'),
    '#description'   =>t('During theme development, it can be very useful to continuously <a href="!link">rebuild the theme registry</a>. WARNING: this is a huge performance penalty and must be turned off on production websites.', array('!link' => 'http://drupal.org/node/173880#theme-registry')),
    '#default_value' => theme_get_setting('clear_registry'),
  );
  $form['aldersgate_settings']['scroll_to_top'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show scroll to top arrow'),
    '#default_value' => theme_get_setting('scroll_to_top'),
    '#description'   => t("Check this option to enable the Scroll to Top javascript. Uncheck to hide."),
  );
  $form['aldersgate_settings']['breadcrumbs'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show breadcrumbs in a page'),
    '#default_value' => theme_get_setting('breadcrumbs'),
    '#description'   => t("Check this option to show breadcrumbs in page. Uncheck to hide."),
  );
  
  
  
  $form['aldersgate_settings']['region'] = array(
    '#type' => 'fieldset',
    '#title' => t('Header & Region Settings'),
    '#description' => t('Modify the region background images & parallax scrolling options'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );

  /*** TITLE WRAP REGION SETTINGS **************************************************/
  $form['aldersgate_settings']['region']['title_wrap'] = array(
    '#type' => 'fieldset',
    '#title' => t('Page Title Wrapper'),
    '#description' => t('Modify the title wrapper settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    );
  $form['aldersgate_settings']['region']['title_wrap']['title_wrap_use_default_image'] = array(
    '#title' => t('Use the default image'),
    '#type' => 'checkbox',
    '#description' => t('Check here if you want to use the default background image for this region'),
    '#default_value' => theme_get_setting('title_wrap_use_default_image'),
    );
  $form['aldersgate_settings']['region']['title_wrap']['title_wrap_image'] = array(
    '#title' => t('Title Wrapper Background Image'),
    '#type' => 'managed_file',
    '#description' => t('The uploaded image will be displayed as the background of the specified region.'),
    '#default_value' => theme_get_setting('title_wrap_image'),
    '#upload_location' => 'public://aldersgate_theme/',
    '#states' => array(
      'visible' => array(
        ':input[name="title_wrap_use_default_image"]' => array('checked' => FALSE),
        ),
      ),
    );
  $form['aldersgate_settings']['region']['title_wrap']['title_wrap_parallax_enable'] = array(
    '#title' => t('Enable parallax scrolling effect'),
    '#type' => 'checkbox',
    '#description' => t('Check here if you want to enable the parallax scrolling effect'),
    '#default_value' => theme_get_setting('title_wrap_parallax_enable'),
    );
  $form['aldersgate_settings']['region']['title_wrap']['title_wrap_parallax_speed'] = array(
    '#title' => t('Set the speed of the scrolling effect'),
    '#type' => 'textfield',
    '#description' => t('The speed at which the parallax effect runs. 0.0 means the image will appear fixed in place, and 1.0 the image will flow at the same speed as the page content. (<em>Default = 0.2</em>)'),
    '#default_value' => theme_get_setting('title_wrap_parallax_speed'),
    '#size'=>'5',
    '#states' => array(
      'visible' => array(
        ':input[name="title_wrap_parallax_enable"]' => array('checked' => TRUE),
        ),
      ),
    );
  $form['aldersgate_settings']['region']['title_wrap']['title_wrap_parallax_position'] = array(
    '#title' => t('Set the position of the background image'),
    '#type' => 'textfield',
    '#description' => t('This is analogous to the background-position css property. Specify coordinates as top, bottom, right, left, center, or pixel values (e.g. -10px 0px). The parallax image will be positioned as close to these values as possible while still covering the target element. (<em>Default = 0 -50px</em>)'),
    '#default_value' => theme_get_setting('title_wrap_parallax_position'),
    '#size'=>'5',
    '#states' => array(
      'visible' => array(
        ':input[name="title_wrap_parallax_enable"]' => array('checked' => TRUE),
      ),
    ),
    );



  /*** HIGHLIGHT REGION SETTINGS **************************************************/
  $form['aldersgate_settings']['region']['highlight'] = array(
    '#type' => 'fieldset',
    '#title' => t('Highlight Region'),
    '#description' => t('Modify the "highlight" region settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    );
  $form['aldersgate_settings']['region']['highlight']['highlight_use_default_image'] = array(
    '#title' => t('Use the default image'),
    '#type' => 'checkbox',
    '#description' => t('Check here if you want to use the default background image for this region'),
    '#default_value' => theme_get_setting('highlight_use_default_image'),
    );
  $form['aldersgate_settings']['region']['highlight']['highlight_image'] = array(
    '#title' => t('Highlight Region Background Image'),
    '#type' => 'managed_file',
    '#description' => t('The uploaded image will be displayed as the background of the specified region.'),
    '#default_value' => theme_get_setting('highlight_image'),
    '#upload_location' => 'public://aldersgate_theme/',
    '#states' => array(
      'visible' => array(
        ':input[name="highlight_use_default_image"]' => array('checked' => FALSE),
      ),
    ),
    );
  $form['aldersgate_settings']['region']['highlight']['highlight_parallax_enable'] = array(
    '#title' => t('Enable parallax scrolling effect'),
    '#type' => 'checkbox',
    '#description' => t('Check here if you want to enable the parallax scrolling effect'),
    '#default_value' => theme_get_setting('highlight_parallax_enable'),
    );
  $form['aldersgate_settings']['region']['highlight']['highlight_parallax_speed'] = array(
    '#title' => t('Set the speed of the scrolling effect'),
    '#type' => 'textfield',
    '#description' => t('The speed at which the parallax effect runs. 0.0 means the image will appear fixed in place, and 1.0 the image will flow at the same speed as the page content. (<em>Default = 0.2</em>)'),
    '#default_value' => theme_get_setting('highlight_parallax_speed'),
    '#size'=>'5',
    '#states' => array(
      'visible' => array(
        ':input[name="highlight_parallax_enable"]' => array('checked' => TRUE),
      ),
    ),
    );
  $form['aldersgate_settings']['region']['highlight']['highlight_parallax_position'] = array(
    '#title' => t('Set the position of the background image'),
    '#type' => 'textfield',
    '#description' => t('This is analogous to the background-position css property. Specify coordinates as top, bottom, right, left, center, or pixel values (e.g. -10px 0px). The parallax image will be positioned as close to these values as possible while still covering the target element. (<em>Default = 0 0</em>)'),
    '#default_value' => theme_get_setting('highlight_parallax_position'),
    '#size'=>'5',
    '#states' => array(
      'visible' => array(
        ':input[name="highlight_parallax_enable"]' => array('checked' => TRUE),
      ),
    ),
    );
  
  
  /*** BOTTOM_HIGHLIGHT_1 REGION SETTINGS **************************************************/
  $form['aldersgate_settings']['region']['bottom_highlight1'] = array(
    '#type' => 'fieldset',
    '#title' => t('Bottom Highlight1 Region'),
    '#description' => t('Modify the "bottom_highlight1" region settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    );
  $form['aldersgate_settings']['region']['bottom_highlight1']['bottom_highlight1_use_default_image'] = array(
    '#title' => t('Use the default image'),
    '#type' => 'checkbox',
    '#description' => t('Check here if you want to use the default background image for this region'),
    '#default_value' => theme_get_setting('bottom_highlight1_use_default_image'),
    );
  $form['aldersgate_settings']['region']['bottom_highlight1']['bottom_highlight1_image'] = array(
    '#title' => t('Bottom Highlight 1 Region Background Image'),
    '#type' => 'managed_file',
    '#description' => t('The uploaded image will be displayed as the background of the specified region.'),
    '#default_value' => theme_get_setting('bottom_highlight1_image'),
    '#upload_location' => 'public://aldersgate_theme/',
    '#states' => array(
      'visible' => array(
        ':input[name="bottom_highlight1_use_default_image"]' => array('checked' => FALSE),
      ),
    ),
    );
  $form['aldersgate_settings']['region']['bottom_highlight1']['bottom_highlight1_parallax_enable'] = array(
    '#title' => t('Enable parallax scrolling effect'),
    '#type' => 'checkbox',
    '#description' => t('Check here if you want to enable the parallax scrolling effect'),
    '#default_value' => theme_get_setting('bottom_highlight1_parallax_enable'),
    );
  $form['aldersgate_settings']['region']['bottom_highlight1']['bottom_highlight1_parallax_speed'] = array(
    '#title' => t('Set the speed of the scrolling effect'),
    '#type' => 'textfield',
    '#description' => t('The speed at which the parallax effect runs. 0.0 means the image will appear fixed in place, and 1.0 the image will flow at the same speed as the page content. (<em>Default = 0.2</em>)'),
    '#default_value' => theme_get_setting('bottom_highlight1_parallax_speed'),
    '#size'=>'5',
    '#states' => array(
      'visible' => array(
        ':input[name="bottom_highlight1_parallax_enable"]' => array('checked' => TRUE),
      ),
    ),
    );
  $form['aldersgate_settings']['region']['bottom_highlight1']['bottom_highlight1_parallax_position'] = array(
    '#title' => t('Set the position of the background image'),
    '#type' => 'textfield',
    '#description' => t('This is analogous to the background-position css property. Specify coordinates as top, bottom, right, left, center, or pixel values (e.g. -10px 0px). The parallax image will be positioned as close to these values as possible while still covering the target element. (<em>Default = 0 0</em>)'),
    '#default_value' => theme_get_setting('bottom_highlight1_parallax_position'),
    '#size'=>'5',
    '#states' => array(
      'visible' => array(
        ':input[name="bottom_highlight1_parallax_enable"]' => array('checked' => TRUE),
      ),
    ),
    );
  
  /*** BOTTOM_HIGHLIGHT_2 REGION SETTINGS **************************************************/
  $form['aldersgate_settings']['region']['bottom_highlight2'] = array(
    '#type' => 'fieldset',
    '#title' => t('Bottom Highlight2 Region'),
    '#description' => t('Modify the "bottom_highlight2" region settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    );
  $form['aldersgate_settings']['region']['bottom_highlight2']['bottom_highlight2_use_default_image'] = array(
    '#title' => t('Use the default image'),
    '#type' => 'checkbox',
    '#description' => t('Check here if you want to use the default background image for this region'),
    '#default_value' => theme_get_setting('bottom_highlight2_use_default_image'),
    );
  $form['aldersgate_settings']['region']['bottom_highlight2']['bottom_highlight2_image'] = array(
    '#title' => t('Bottom Highlight 2 Region Background Image'),
    '#type' => 'managed_file',
    '#description' => t('The uploaded image will be displayed as the background of the specified region.'),
    '#default_value' => theme_get_setting('bottom_highlight2_image'),
    '#upload_location' => 'public://aldersgate_theme/',
    '#states' => array(
      'visible' => array(
        ':input[name="bottom_highlight2_use_default_image"]' => array('checked' => FALSE),
      ),
    ), 
    );
  $form['aldersgate_settings']['region']['bottom_highlight2']['bottom_highlight2_parallax_enable'] = array(
    '#title' => t('Enable parallax scrolling effect'),
    '#type' => 'checkbox',
    '#description' => t('Check here if you want to enable the parallax scrolling effect'),
    '#default_value' => theme_get_setting('bottom_highlight2_parallax_enable'),
    );
  $form['aldersgate_settings']['region']['bottom_highlight2']['bottom_highlight2_parallax_speed'] = array(
    '#title' => t('Set the speed of the scrolling effect'),
    '#type' => 'textfield',
    '#description' => t('The speed at which the parallax effect runs. 0.0 means the image will appear fixed in place, and 1.0 the image will flow at the same speed as the page content. (<em>Default = 0.2</em>)'),
    '#default_value' => theme_get_setting('bottom_highlight2_parallax_speed'),
    '#size'=>'5',
    '#states' => array(
      'visible' => array(
        ':input[name="bottom_highlight2_parallax_enable"]' => array('checked' => TRUE),
      ),
    ),
    );
  $form['aldersgate_settings']['region']['bottom_highlight2']['bottom_highlight2_parallax_position'] = array(
    '#title' => t('Set the position of the background image'),
    '#type' => 'textfield',
    '#description' => t('This is analogous to the background-position css property. Specify coordinates as top, bottom, right, left, center, or pixel values (e.g. -10px 0px). The parallax image will be positioned as close to these values as possible while still covering the target element. (<em>Default = 0 0</em>)'),
    '#default_value' => theme_get_setting('bottom_highlight2_parallax_position'),
    '#size'=>'5',
    '#states' => array(
      'visible' => array(
        ':input[name="bottom_highlight2_parallax_enable"]' => array('checked' => TRUE),
      ),
    ),
    );
  
  
  
  $form['aldersgate_settings']['socialicon'] = array(
    '#type' => 'fieldset',
    '#title' => t('Social Icons'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['aldersgate_settings']['socialicon']['display_social_icons'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show Social Icons'),
    '#default_value' => theme_get_setting('display_social_icons'),
    '#description'   => t("Select this option to display social media icons"),
  );
  $form['aldersgate_settings']['socialicon']['twitter_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Twitter Profile'),
    '#default_value' => theme_get_setting('twitter_url'),
    '#description'   => t("Enter your Twitter Profile URL. Leave blank to hide."),
  );
  $form['aldersgate_settings']['socialicon']['facebook_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Facebook Profile'),
    '#default_value' => theme_get_setting('facebook_url'),
    '#description'   => t("Enter your Facebook Profile URL. Leave blank to hide."),
  );
  $form['aldersgate_settings']['socialicon']['google_plus_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Google+ Profile'),
    '#default_value' => theme_get_setting('google_plus_url'),
    '#description'   => t("Enter your Google Plus URL. Leave blank to hide."),
  );
  $form['aldersgate_settings']['socialicon']['pinterest_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Pinterest'),
    '#default_value' => theme_get_setting('pinterest_url'),
    '#description'   => t("Enter your Pinterest URL. Leave blank to hide."),
  );
}
